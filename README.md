# DF Test Mod

A mod for [Dwarf_Fortress][Dwarf_Fortress] with the main purpose of testing syntax
for the [DF Language Server][DF_LS].

We hope this mod combined with the guides will help us and the community.
With this we can improve and debug our tools.
This mod can also be used for examples in issues or teach people about modding.

## Guides

- [General info about DF mods](./guilds/general_info_mod.md)
- [Install a/this mod](./guilds/install_mod.md)
- [Create your own mod](./guilds/install_mod.md)
- [Debug a mod](./guilds/debug_mod.md)

If you have suggestions, changes, questions,... please let is know on
[Discord][Discord] or [open an issue](https://gitlab.com/df-modding-tools/df-test-mod/-/issues/new).

## Tools
We (the DF Modding Tools team/contributes) have created some tools for modding.
Here are the most important:

- [DF Language Server and extensions](https://gitlab.com/df-modding-tools/df-raw-language-server#supported-editors-and-ides)
- We have [more ideas](https://gitlab.com/df-modding-tools/df-raw-language-server/-/blob/dev/Roadmap.md), but currently not planned.

## Other resources

- [DF Modding tools Discord][Discord]
- [DF Wiki](https://dwarffortresswiki.org/index.php/Main_Page)
- [DF Wiki Modding guide](https://dwarffortresswiki.org/index.php/DF2014:Modding#Guide)
- [DF Modding Forum](http://www.bay12forums.com/smf/index.php?board=13.0)
- [Kitfox Discord](https://discord.gg/kitfoxgames)
- [DF Community Discord](https://discord.gg/7XqEDDuJq5)

## License
The code in this project is licensed under the MIT or Apache 2.0 license.
(So feel free to use this to create your own mods.)

All documentation is licensed under
[GNU FDL](https://www.gnu.org/licenses/fdl-1.3.html),
[MIT license](https://choosealicense.com/licenses/mit/) and/or
[Creative Commons Attribution-ShareAlike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/) 

This makes the documentation both compatible with the
[Dwarf Fortress Wiki](https://dwarffortresswiki.org) and [Wikipedia](https://www.wikipedia.org/).

All contributions, code and documentation, to this project will be similarly licensed.


[Discord]: https://discord.gg/6eKf5ZY
[VSCode_Marketplace]: https://marketplace.visualstudio.com/items?itemName=df-modding-tools.dwarf-fortress-raw-vscode
[DF_LS]: https://gitlab.com/df-modding-tools/df-raw-language-server
[Dwarf_Fortress]: https://bay12games.com/dwarves/